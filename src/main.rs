#![allow(unused_parens)]

fn main() {
    let days = [
        "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth",
        "10th", "11th", "12th",
    ];
    let vers = [
        "12 drummers drumming",
        "11 pipers piping",
        "10 lords a-leaping",
        "Nine ladies dancing",
        "Eight maids a-milking",
        "Seven swans a-swimming",
        "Six geese a-laying",
        "Five golden rings",
        "Four calling birds",
        "Three french hens",
        "Two turtles doves, and",
        "A Partridge in a Pear Tree",
    ];
    let mut index = 0;

    for elem in days.iter() {
        println!("On the {} day of Christmas my true love sent to me", elem);
        for i in (11 - index..12) {
            println!("{}", vers[i]);
        }
        println!("");
        index += 1;
    }
}
